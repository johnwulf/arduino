

/****************************
 * GetRelativeHumidity
 *  An example sketch that reads the sensor and prints the
 *  relative humidity to the PC's serial port
 *
 *  Tested with the SHT21-Breakout
 *  Humidity sensor from Modern Device.
 *****************************/
#include <Wire.h>
#include <LibHumidity.h>
#include <LCD4Bit_mod.h>

LCD4Bit_mod lcd = LCD4Bit_mod(2);

LibHumidity humidity = LibHumidity();

void setup() {
  Serial.begin(9600);
  digitalWrite(3, LOW);
  pinMode(3, OUTPUT);
  
  lcd.init();
  lcd.clear();
  lcd.cursorTo(1,0);
  lcd.printIn("Temparture Monster");
  
  pinMode(10, OUTPUT);
}


void loop() {
  Serial.println("Use standard config **************");
  read();
  delay(1000);

  digitalWrite(3, HIGH);
      
  Serial.print("Use 12/14 config ************** Value:");
  Serial.println(humidity.setResolution(RES_12_14), BIN);
  read();
  delay(1000);


  Serial.print("Use 10/13 config ************** Value");
  Serial.println(humidity.setResolution(RES_10_13), BIN);
  read();
  delay(1000);
  digitalWrite(3, LOW);

  Serial.print("Use  8/12 config ************** Value:");
  Serial.println(humidity.setResolution(RES_08_12), BIN);
  read();
  delay(1000);
  digitalWrite(10,LOW);

  Serial.print("Set 11/11 config ************** Value: ");
  Serial.println(humidity.setResolution(RES_11_11), BIN);
  read();
  delay(1000);
  digitalWrite(3, HIGH);
  
  Serial.println("Resetting sensor");
  humidity.reset();
  delay(1000);

  digitalWrite(3, LOW);
  digitalWrite(10,HIGH);
}

void read() {
  Serial.print(" RHumidity: ");
  Serial.print(humidity.getHumidity());
  Serial.print(" Temp in C: ");
  Serial.print(humidity.getTemperature(true));
  Serial.print(" Temp in F: ");
  Serial.println(humidity.getTemperature(false));
  Serial.print(" URegister: ");
  Serial.println(humidity.getUserRegister(), BIN);
}
