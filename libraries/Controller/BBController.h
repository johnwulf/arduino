/*
 BBController - Implementation of Bang Bang controller
 */


#ifndef _PIDCONTROLLER_H_
#define _PIDCONTROLLER_H_


class BBController {
public:
  enum Mode { NEUTRAL = 0, HEATING = 1, COOLING = 2 };
  enum State { OFF = 0, CONTROL_WAIT = 1, CONTROL_ON = 2 };

  
  BBController(int uMax, unsigned long deltaTime, Mode mode);
  
  /*!
   * targetControlTimeRatio the ratio between how long it takes to reach the lower control boundary
   *                        when no energy is applied (cooling down) to how long it takes to reach the
   *                        setpoint when energy is applied.
   */
  void updateSetpoint(float setpoint, float tolerance, float targetTime, float targetControlTimeRatio, float y);

  // Do we need that as an extra method?
  void reset(float y);
  
  int calculateOutput(float y);
  
  float getLastY();
  
  void printDebug();
  
private:
  // Limits for controll function
  int uMax;
  int u;
  
  
  // setpoint
  float setpoint;
  float tolerance;
  
  // timing parameters
  float targetTime;
  float targetControlTimeRatio;
  float sampleTime;
  
  float timeSinceLastSample;
  float timePassed;
  
  // running parameters
  State state;
  Mode mode;
  
  float lastY;
  
  unsigned long lastMillis;
  
  boolean increaseControlValue(float y);
};

#endif
