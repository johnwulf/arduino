/*
  HVAC.cpp - library implementation for controlling a HVAC
*/

#include "HVAC.h"

// Delay to let the HVAC settle after power is switched on or off
#define HVAC_SWITCH_DELAY 500 
#define MODE_SENSOR_THRESHOLD 10 // At least 0.25A have been detected

const int HVAC::_states[2][4][4] = {
      {
        { LOW , LOW , LOW , LOW  },
        { HIGH, LOW , LOW , LOW  }, 
        { HIGH, LOW , HIGH, LOW  },
        { HIGH, LOW , HIGH, HIGH }
      },
      {	
        { LOW , LOW , LOW , LOW  },
        { LOW , HIGH, LOW , LOW  }, 
        { LOW , HIGH, HIGH, LOW  },
        { LOW , HIGH, HIGH, HIGH }
      }
    };

HVAC::HVAC(int heatPowerPin, int coolPowerPin, int fanLowPin, int fanHighPin, int modePowerPin, int modeSensorPin) {
  _pins[HEAT_PIN] = heatPowerPin;
  _pins[COOL_PIN] = coolPowerPin;
  _pins[FAN_LOW_PIN] = fanLowPin;
  _pins[FAN_HIGH_PIN] = fanHighPin;
  
  _modePowerPin = modePowerPin;
  _modeSensorPin = modeSensorPin;

  _mode = UNKNOWN;
  _fanLevel = FAN_LOW;
  _power = false;

  initPins();
  setPins();
}

HVAC::~HVAC() {
  // nothing here
}


bool HVAC::power() {
  _power = !_power;
  setPins();

  return _power;
}

void HVAC::on() {
  _power = true;
  setPins();
}

void HVAC::off() {
  _power = false;
  setPins();
}

bool HVAC::isOn() {
  return _power;
}

void HVAC::low() {
  setFanLevel(FAN_LOW);
}

void HVAC::medium() {
  setFanLevel(FAN_MEDIUM);
}

void HVAC::high() {
  setFanLevel(FAN_HIGH);
}

HVAC::FanLevel HVAC::getFanLevel() {
  return _power ? _fanLevel : FAN_OFF;
}

void HVAC::setFanLevel(HVAC::FanLevel level) {
  if(level == FAN_OFF) {
    off();
    _fanLevel = FAN_LOW;
  }
  else {
    _fanLevel = level > FAN_HIGH ? FAN_HIGH : (level < 0 ? FAN_OFF : level);
    on();
  }
}

HVAC::Mode HVAC::getMode() {
  return _mode;
}

// private methods
void HVAC::initPins() {
  for(int i = 0; i < MAX_PIN; i++) {
    digitalWrite(_pins[i], LOW);
    pinMode(_pins[i], OUTPUT);
  }
  
  // TODO need to work on mode pin
  digitalWrite(_modePowerPin, LOW);
  if(_modePowerPin > -1) {
    pinMode(_modePowerPin, OUTPUT);
    pinMode(_modeSensorPin, INPUT);
  }
}

void HVAC::setMode(Mode mode) {
  _mode = mode;
}

// The process is to turn on cool and detect
// the current going to the fan. Only if the 
// system is set to colling, the fan will turn on
// and a current will be detected. 
void HVAC::detectMode() {
  int currentOff;
  int currentOn;
  
  // Do this only if we have a sensor
  if(_modePowerPin > -1) {
    // Step 1: Save current state
    Mode tmpMode = _mode;
    FanLevel tmpFanLevel = _fanLevel;
    bool tmpPower = _power;
    
    // Step 2: Calibrate. Turn off power, turn on sensor, measure current
    off();
    
    // Turn sensor on and let it ramp up
    digitalWrite(_modePowerPin, HIGH);
    
    // if power was off, we can speed this up
    if(tmpPower) {
      // If we actually turned off the power, i.e. it was on, give it a standard
      // delay to settle.
      delay(HVAC_SWITCH_DELAY);
    }
    else {
      // This is just for the sensor to settle, probably not needed
      delay(5);
    }

    currentOff = senseCurrent();
    
    // Step 3: Set mode to cool and start the fan
    _mode = COOL;
    high();
    on();
    delay(HVAC_SWITCH_DELAY); // Give it some time to start
    
    currentOn = senseCurrent();
    
    // Step 4: derive mode
    if((currentOn - currentOff) > MODE_SENSOR_THRESHOLD) {
      _mode = COOL;
    }
    else {
      _mode = HEAT;
    }

    // Step 5: Reset everything
    // Turn sensor off as we are done; reset modes; and turn power off if it was off
    digitalWrite(_modePowerPin, LOW);
    _fanLevel = tmpFanLevel;

    if(tmpPower) {
      on();
    }
    else {
      off();
    }
  }
}

// senseCurrent expects the sensor to be turned on.
int HVAC::senseCurrent() {
  int current = 0;
  int total = 0;
  int maxCurrent = 0;
  int minCurrent = 10000;
  const int numMeasurements = 20;
  
  // Let's take 20 measurements and average them
  for(int i = 0; i < numMeasurements; i++) {
    current = analogRead(_modeSensorPin);
    if(maxCurrent < current) {
      maxCurrent = current;
    }
    if(minCurrent > current) {
      minCurrent = current;
    }
    
    total += current;
    // Wait for 20 ms
    delay(20);
  }
  
  // Take out the outliers
  total -= (minCurrent + maxCurrent);
  
  return total / (numMeasurements - 2);
}

void HVAC::setPins() {
  int state = FAN_OFF;
  int mode = HEAT;

  // Only if the HVAC is powered on we will
  // utilize the power level. Otherwise, we 
  // will use the off state.
  if(_power) {
    state = _fanLevel;
  }

  
  // Only if the mode is not unknown, we utilize the mode.
  // Otherwise we use heat as default.
  if(_mode != UNKNOWN) {
    mode = _mode;
  }

  for(int i = 0; i < MAX_PIN; i++) {
    digitalWrite(_pins[i], _states[mode][state][i]);
  }
}

void HVAC::printDebug() {
  Serial.print(" power:");
  if(_power) {
    Serial.print("true");    
  } 
  else {
    Serial.print("false");        
  }
  Serial.print(", fanLevel:");
  Serial.print(_fanLevel);
  Serial.print(", mode:");
  Serial.print(_mode);
}

