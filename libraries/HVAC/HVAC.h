/*
 HVAC.h - library for controlling a HVAC
*/

#ifndef __HVAC_H__
#define __HVAC_H__

#include "WProgram.h"



class HVAC {
public:
  enum FanLevel { FAN_OFF = 0, FAN_LOW = 1, FAN_MEDIUM = 2, FAN_HIGH = 3 };
  
  enum Mode { HEAT = 0, COOL = 1, UNKNOWN = 2 };
  
  enum Pins { HEAT_PIN = 0, COOL_PIN = 1, FAN_LOW_PIN = 2, FAN_HIGH_PIN = 3, MAX_PIN};
  
  HVAC(int heatPowerPin, int coolPowerPin, int fanLowPin, int fanHighPin, int modePowerPin, int modeSensorPin);
  ~HVAC();
  
  bool power();
  void on();
  void off();
  bool isOn();
  
  void setFanLevel(FanLevel level);
  FanLevel getFanLevel();

  void low();
  void medium();
  void high();
  
  void setMode(Mode mode);
  void detectMode();
  Mode getMode();
  
  void printDebug();

private:

  void initPins();
  void setPins();
  int senseCurrent();

  // Attributes
  static const int _states[2][4][MAX_PIN];
  
  int _pins[MAX_PIN];
  int _modePowerPin;
  int _modeSensorPin;
  
  Mode _mode;
  FanLevel _fanLevel;
  bool _power;
};
#endif

