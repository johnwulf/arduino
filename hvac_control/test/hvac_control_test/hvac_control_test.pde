#define __DEBUG__ 1

#include <Wire.h>
#include <HVAC.h>
#include <LibHumidity.h>
#include <LCD4Bit_mod.h>
#include <BBController.h>


const int HEAT_PIN = 4;
const int COOL_PIN = 5;

const int FAN_LOW_PIN = 6;
const int FAN_HIGH_PIN = 7;

const int MODE_POWER_PIN = -1;
const int MODE_SENSOR_PIN = -1;

HVAC hvac(HEAT_PIN, COOL_PIN, FAN_LOW_PIN, FAN_HIGH_PIN, MODE_POWER_PIN, MODE_SENSOR_PIN);  
LibHumidity tempSensor;
BBController controller = BBController(3, 90, BBController::COOLING);


float desiredTemperature = 71;
float tolerance = 2;
float halfTime = 3600;
float controlTimeRatio = 0.5;
float temperature = 0;
float humidity = 0;
unsigned long lastMillis;
float h;

void setup() {
  Serial.begin(9600);
  
  Serial.println("Setting temperature sensor resolution");
  tempSensor.setResolution(RES_12_14);

  lastMillis = millis(); 
  
  Serial.println("initializing control point");
  controller.updateSetpoint(desiredTemperature, tolerance, halfTime, controlTimeRatio, tempSensor.getTemperature(false));
  hvac.setMode(HVAC::COOL);
  delay(1000);
  Serial.println("Done initializing");
}

void loop() {
  float u;
  unsigned long currentMillis = millis();
  int sleepTime;
  
  // Time difference
  h = currentMillis - lastMillis;
  
  // Get process variables
  temperature = tempSensor.getTemperature(false);
  humidity = tempSensor.getHumidity();

  // if the setpoint has changed, update it, and calculate new fan setting
  u = controller.calculateOutput(temperature);
  hvac.setFanLevel((HVAC::FanLevel)u);
  // Read the keyboard
  if(Serial.available() > 0) {
    int c = Serial.read();
    switch(c) {
      case 112:
        hvac.on();
        break;
      case 111:
        hvac.off();
        break;
      case 108:
        hvac.low();
        break;
      case 109:
        hvac.medium();
        break;
      case 104:
        hvac.high();
        break;
      case 48:
      case 49:
      case 50:
      case 51:
      case 52:
      case 53:
        hvac.setFanLevel((HVAC::FanLevel)(c - 49));
        break;
      case 43:
        desiredTemperature += 2;
         controller.updateSetpoint(desiredTemperature, tolerance, halfTime, controlTimeRatio, tempSensor.getTemperature(false));
        break;
      case 45:
        desiredTemperature -= 2;
         controller.updateSetpoint(desiredTemperature, tolerance, halfTime, controlTimeRatio, tempSensor.getTemperature(false));
        break;
    }    
  }  
  lastMillis = currentMillis;
 
  printDebug();
  
  sleepTime = 1000; // - (millis() - currentMillis);
  delay(1000); 
}


inline void printDebug() {
#ifdef __DEBUG__
    Serial.print("millis:");
    Serial.print(lastMillis);
    Serial.print(", h:");
    Serial.print(h);
    Serial.print(", temp:");
    Serial.print(temperature);
    Serial.print(", humidity:");
    Serial.print(humidity);
    Serial.print(",");
    controller.printDebug();
    Serial.print(",");
    hvac.printDebug();
    Serial.println();
#endif 
}  
